export interface IDbCountRecord {
    count: number;
}

export const DB_COUNT_RECORD_KEY = 'count';
