export interface IDbExistsRecord {
    exists: boolean;
}

export const DB_EXISTS_RECORD_KEY = 'exists';
