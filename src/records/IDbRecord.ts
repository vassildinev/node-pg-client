import { KeyNameMap, Nullable } from "typescript-helper-types";

export interface IDbRecord {
    id: string;
    created_at: Date;
    last_updated_at: Nullable<Date>;
    deleted_at: Nullable<Date>;
}

export const DbRecordKeys: KeyNameMap<IDbRecord> = {
    created_at: "created_at",
    deleted_at: "deleted_at",
    id: "id",
    last_updated_at: "last_updated_at",
};
