import { v4 } from "uuid";

import { IDbAction, IDbClient, IPgClient, IPgPoolClient, IQueryConfig, IQueryResult } from "./contracts";
import { DbError } from "./DbError";

export class DbClient implements IDbClient {
    constructor(private readonly pgPool: IPgPoolClient) { }

    async executeQuery<T>(queryConfig: IQueryConfig): Promise<IQueryResult<T>> {
        return await this.queryWithClient(this.pgPool, queryConfig);
    }

    async runTransaction<T>(action: IDbAction<T>): Promise<T> {
        const txClient = await this.pgPool.connect();

        try {
            await txClient.query('BEGIN');

            const result = await this.executeDbActionWithClient<T>(action, txClient);

            await txClient.query('COMMIT');

            return result;
        } catch (err) {
            await txClient.query('ROLLBACK');

            if (!DbError.isInstance(err)) {
                throw new DbError(err);
            } else {
                throw err;
            }
        } finally {
            txClient.release();
        }
    }

    private async executeDbActionWithClient<T>(action: IDbAction<T>, txClient: IPgClient) {
        return await action({
            executeQuery: q => this.queryWithClient(txClient, q),
            runTransaction: a => this.innerTransactionWithSavePoint(txClient, a),
        });
    }

    private async queryWithClient<T>(pgClient: IPgClient, queryConfig: IQueryConfig): Promise<IQueryResult<T>> {
        try {
            const result = await pgClient.query<T>(queryConfig);
            return result;
        } catch (err) {
            throw new DbError(err, queryConfig);
        }
    }

    private async innerTransactionWithSavePoint<NT>(txClient: IPgClient, innerAction: IDbAction<NT>): Promise<NT> {
        const savePoint = `"${v4()}"`;

        try {
            await txClient.query(`SAVEPOINT ${savePoint}`);

            const nestedResult = this.executeDbActionWithClient(innerAction, txClient);

            await txClient.query(`RELEASE SAVEPOINT ${savePoint}`);

            return nestedResult;
        } catch (err) {
            await txClient.query(`ROLLBACK TO SAVEPOINT ${savePoint}`);
            throw err;
        }
    }
}