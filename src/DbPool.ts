import { Pool } from "pg";

import { IPgPoolClient } from "./contracts";

let pool: IPgPoolClient;

export const createPgPoolClient = (): IPgPoolClient => {
    if (!pool) {
        pool = new Pool();
    }

    return pool;
};
