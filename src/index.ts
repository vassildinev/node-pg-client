import { IDbClientFactory } from './contracts/IDbClientFactory';
import { DbClient } from './DbClient';
import { createPgPoolClient } from './DbPool';

export * from './contracts';
export * from './records';
export * from './DbError';

export const createDbClient: IDbClientFactory =
    () => new DbClient(createPgPoolClient());