import { Optional } from 'typescript-helper-types';

import { IQueryConfig } from "./contracts";

export class DbError extends Error {
    constructor(readonly innerError: unknown, readonly query?: Optional<IQueryConfig>) {
        super('An error occurred while executing a db query. See inner error for more details.');

        this.name = DbError.name;
        Error.captureStackTrace(this, DbError);
    }

    static isInstance(err: unknown): err is DbError {
        return err instanceof DbError && err.name === DbError.name;
    }
}