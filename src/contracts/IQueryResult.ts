export interface IQueryResult<T> {
    rows: T[];
    rowCount: number;
}
