import { IQueryConfig } from "./IQueryConfig";
import { IQueryResult } from "./IQueryResult";

export interface IPgPoolClient extends IPgClient {
    connect(): Promise<ITransactionPgClient>;
}

export interface ITransactionPgClient extends IPgClient {
    release(): void;
}

export interface IPgClient {
    query<T>(queryConfig: IQueryConfig): Promise<IQueryResult<T>>;
}