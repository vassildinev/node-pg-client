import { IDbClient } from "./IDbClient";

export type IDbClientFactory = () => IDbClient;