export type IQueryConfig = string | {
    name?: string;
    text: string;
    values?: unknown[];
};
