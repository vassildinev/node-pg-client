import { IQueryConfig } from "./IQueryConfig";
import { IQueryResult } from "./IQueryResult";

export type IDbQueryAction = <T>(queryConfig: IQueryConfig) => Promise<IQueryResult<T>>;
