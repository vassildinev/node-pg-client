import { IDbAction } from "./IDbAction";
import { IQueryConfig } from "./IQueryConfig";
import { IQueryResult } from "./IQueryResult";

export interface IDbClient {
    executeQuery<T>(query: IQueryConfig): Promise<IQueryResult<T>>;
    runTransaction<T>(action: IDbAction<T>): Promise<T>;
}

