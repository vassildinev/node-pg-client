import { IDbClient } from "./IDbClient";

export type IDbAction<T> = (txClient: IDbClient) => Promise<T>;